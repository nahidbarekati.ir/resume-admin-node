export const state = () => ({
  user: {},
  Xtoken: "",
})

export const mutations = {
    setUser(state, data) {
        state.Xtoken = data.token
        state.user =JSON.parse(data.user)
    },
    updateUser(state, user) {
        state.user = user
        localStorage.setItem('local_user',JSON.stringify(user));
    },
}

export const getters = {
    getUser: (state) => {
        return state.user;
    },
};