export const state = () => ({
  breadcrumbs: [],
});

export const mutations = {
  SET_BREADCRUMB(state, payload) {
    state.breadcrumbs = payload;
  },
};

export const getters = {
  breadcrumbs: (state) => {
    return state.breadcrumbs;
  },
  pageTitle: (state) => {
    let last = state.breadcrumbs[state.breadcrumbs.length - 1];
    if (last && last.title) {
      return last.title;
    }
  },
};
