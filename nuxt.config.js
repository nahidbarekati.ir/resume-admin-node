export default {
  server: {
    port: 8585, // default: 3000
    host: "0.0.0.0", // default: localhost
  },

  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: "universal",
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: "server",
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: "khebre",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || "",
      },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
    script: [
      { src: "/js/config.js" },
      { src: "/plugins/global/plugins.bundle.js", ssr: true },
      { src: "/plugins/custom/prismjs/prismjs.bundle.js", ssr: true },
      { src: "/js/scripts.bundle.js", ssr: true },
    ],
  },
  /*
   ** Global CSS
   */
  css: [
    "~assets/plugins/global/plugins.bundle.css",
    "~assets/plugins/custom/prismjs/prismjs.bundle.css",
    "~assets/css/style.bundle.css",
    "~assets/css/custom-rtl.css",
    "~assets/fonts/font-faces.min.css",
    "~assets/fonts/custom-rtl-font.css",
  ],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: ["~/plugins/vee-validate.js"],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    //'@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    "bootstrap-vue/nuxt",
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    // Doc: https://github.com/nuxt/content
    "@nuxt/content",

    "@nuxtjs/apollo",
    "@nuxtjs/auth",
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Content module configuration
   ** See https://content.nuxtjs.org/configuration
   */
  content: {},
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  apollo: {
    // Sets up the apollo client endpoints
    clientConfigs: {
      // recommended: use a file to declare the client configuration (see below for example)
      default: "~/plugins/my-alternative-apollo-config.js",

      // you can setup multiple clients
      alternativeClient: {
        // required
        httpEndpoint: "http://localhost:3000",

        // override HTTP endpoint in browser only
        browserHttpEndpoint: "/graphql",

        // See https://www.apollographql.com/docs/link/links/http.html#options
        httpLinkOptions: {},

        // LocalStorage token
        tokenName: "apollo-token",

        // Enable Automatic Query persisting with Apollo Engine
        persisting: false,

        // Use websockets for everything (no HTTP)
        // You need to pass a `wsEndpoint` for this to work
        websocketsOnly: false,
      },
    },

    /**
     * default 'apollo' definition
     */
    defaultOptions: {
      // See 'apollo' definition
      // For example: default query options
      $query: {
        loadingKey: "loading",
        fetchPolicy: "cache-and-network",
      },
    },

    // setup a global query loader observer (see below for example)
    watchLoading: "~/plugins/apollo-watch-loading-handler.js",

    // setup a global error handler (see below for example)
    errorHandler: "~/plugins/apollo-error-handler.js",

    // Sets the authentication type for any authorized request.
    authenticationType: "Bearer",

    // Token name for the cookie which will be set in case of authentication
    tokenName: "apollo-token",

    // [deprecated] Enable the graphql-tag/loader to parse *.gql/*.graphql files
    includeNodeModules: true,

    // Cookie parameters used to store authentication token
    cookieAttributes: {
      /**
       * Define when the cookie will be removed. Value can be a Number
       * which will be interpreted as days from time of creation or a
       * Date instance. If omitted, the cookie becomes a session cookie.
       */
      expires: 7,

      /**
       * Define the path where the cookie is available. Defaults to '/'
       */
      path: "/",

      /**
       * Define the domain where the cookie is available. Defaults to
       * the domain of the page where the cookie was created.
       */
      domain: "example.com",

      /**
       * A Boolean indicating if the cookie transmission requires a
       * secure protocol (https). Defaults to false.
       */
      secure: false,
    },
  },
  build: {
    /*
     ** You can extend webpack config here
     */
    /*    extend (config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue|gql)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
 */
    transpile: ["vee-validate/dist/rules"],
  },
  auth: {
    redirect: {
      login: "/login",
      logout: "/login",
      callback: "/login",
      home: "/",
    },
    cookie: {
      prefix: "autha.",
      options: {
        path: "/",
      },
    },
    localStorage: {
      prefix: "autha.",
    },
    strategies: {
      local: {
        autoRefresh: {
          enable: true,
        },
        endpoints: {
          login: false,
          logout: false,
          refresh: false,
          user: false,
        },
      },
    },
    tokenRequired: true,
    tokenType: "JWT",
    globalToken: true,
    watchLoggedIn: true,
    rewriteRedirects: false,
    resetOnError: true,
  },
  router: {
    middleware: ["auth"],
  },
};
