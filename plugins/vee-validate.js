import {extend} from 'vee-validate';
import {required, email, image, min, digits, max, integer, regex} from 'vee-validate/dist/rules';

// Add the required rule
extend('required', {
    ...required,
    message: '{_field_} الزامی است'
});

// Add the email rule
extend('email', {
    ...email,
    message: 'قالب پست الکترونیک صحیح نمی‌باشد'
});

// Add the min rule
extend('min', {
    ...min,
    message: '{_field_} وارد شده نباید کمتر از {length} حرف باشد'
});

// Add the max rule
extend('max', {
    ...max,
    message: '{_field_} وارد شده نباید بیشتر از {length} حرف باشد'
});


// Add the digits rule
extend('digits', {
    ...digits,
    message: '{_field_} فقط باید عدد باشد و دقیقا {length} عدد باشد '
});

// Add the image rule
extend('image', {
    ...image,
    message: '{_field_} فقط می تواند شامل تصویر باشد '
});

// Add the regex rule
extend('regex', {
    ...regex,
    message: '{_field_} وارد شده نامعتبر است'
});